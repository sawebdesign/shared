﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace SAWD.WebManagers
{
    
    /// <summary>
    /// Case class for session managers in various systems. By accessing session data via this manager base we have a common entry point and developers do not access the HttpContext session directly.
    /// We are also shadowing different session using this base. We can quite easily change the place where Session data is stored just by changing the LocalContext property below 
    /// </summary>
    public abstract class SessionManagerBase
    {

        public static string SessionID {
            get { return LocalContext.Session.SessionID; }
        }

        /// <summary>
        /// Base method that sets a session variable
        /// </summary>
        /// <param name="name">The name of the session variable</param>
        /// <param name="value">The value to be placed in the variable</param>
        protected static void SetSession(string name, object value) {

            if (LocalContext.Session[name] == null)
                LocalContext.Session.Add(name, value);
           else
                LocalContext.Session[name] = value;
          
        }

        /// <summary>
        /// Base method that sets a session variable serialization
        /// </summary>
        /// <param name="name">The name of the session variable</param>
        /// <param name="value">The value to be placed in the variable</param>
        /// <param name="serialize">Bool to serialize using System.Runtime.Serialization.Json.DataContractJsonSerializer</param>
        public static void SetSession(string name, object value, bool serialize) {
            if (!serialize) {
                SetSession(name, value);
            } else {
                if (value != null) {
                    System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(value.GetType());
                    System.IO.MemoryStream ms = new System.IO.MemoryStream();
                    serializer.WriteObject(ms, value);
                    string json = Encoding.Default.GetString(ms.ToArray());
                    SetSession(name, json);
                } else {
                    SetSession(name, value);
                }
            }
        }
        
        /// <summary>
        /// Returns a value in the session based on the name
        /// </summary>
        /// <param name="name">The name of the session variable</param>
        /// <returns>An object</returns>
        protected static object GetSession(string name)
        {
            return GetSession(name, null);
        }

        /// <summary>
        /// Returns a value in the session based on the name
        /// </summary>
        /// <param name="name">The name of the session variable</param>
        /// <param name="defaultvalue">A default value to return if the session value is null</param>
        /// <returns>An object</returns>
        protected static object GetSession(string name, object defaultvalue)
        {
            //in case we are in an environment where the Session may not exist
            if (LocalContext.Session == null) {
                if (defaultvalue != null)
                    return defaultvalue;
                else
                    return null;
            }
            if (LocalContext.Session[name] == null && defaultvalue != null)
                return defaultvalue;
            return LocalContext.Session[name];
        }

        /// <summary>
        /// Returns a value in the session based on the name
        /// </summary>
        /// <param name="name">The name of the session variable</param>
        /// <param name="serialize">Bool to deserialize using System.Runtime.Serialization.Json.DataContractJsonSerializer</param>
        /// <returns>An object</returns>
        public static T GetSession<T>(string name, bool deserialize) {
            if (!deserialize) {
                return (T)GetSession(name);
            } else {
                string json = (string)GetSession(name);
                if (!string.IsNullOrEmpty(json)) {
                    System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(T));
                    System.IO.MemoryStream ms = new System.IO.MemoryStream(Encoding.UTF8.GetBytes(json));
                    return (T)serializer.ReadObject(ms);
                }
                return (T)GetSession(name);
            }
        }

        /// <summary>
        /// In the case of HttpSession it clears all items from the session. In the case of CacheSession it deletes the simulated session object from the cache
        /// </summary>
        public static void ClearSession() {
            LocalContext.Session.Clear();            
        }

        /// <summary>
        /// In the case of HttpSession it destroys the session completely. In the case of CacheSession it deletes the simulated session object from the cache
        /// </summary>
        public static void AbandonSession() {
            LocalContext.Session.Abandon();
        }

        /// <summary>
        /// Determines what context to be used for the Session. If there is a sessionid in the request, we assume that we are not storing session information in the standard
        /// HttpContext session and instead we use the SAWD.WebManagers.FileContext's session - which is session data stored in a cached entry.
        /// </summary>
        public static IContext LocalContext {
            get {
                IContext context;
                //check to see if we've passed a sessionid param in the request, in which case we know to use session data stored elsewhere as opposed to the server Session
                if (System.Web.HttpContext.Current.Request["sessionid"] == null) {
                    context = new SAWD.WebManagers.SAWDHttpContext();
                }
                else {
                    context = new SAWD.WebManagers.CacheSessionContext();
                }
                return context;
            }
        }

    }
}
