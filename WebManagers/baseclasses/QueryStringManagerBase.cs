﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Reflection;

namespace SAWD.WebManagers
{
    /// <summary>
    /// A class used as a common entry point for accessing the querystring. No direct access to the querystring object should be used in code that uses this class Before a property is added to this class permission must be granted by the 
    /// "standards police".
    /// </summary>
    public abstract class QueryStringManagerBase 
    {

        protected QueryStringManagerBase()
        {

        }


        /// <summary>
        /// Casts the value part of a querystring paramater to a string and returns that value 
        /// </summary>
        /// <param name="name">The name of the querystring param</param>
        /// <returns>Returns a valid string value or null if the parameter did not exist in the querystring</returns>
        protected virtual string GetQuerystringParam_string(string name)
        {
            HttpContext context = HttpContext.Current;

            if (context.Request.QueryString[name] == null)
                return null;
            return context.Request.QueryString[name].ToString();
        }


        /// <summary>
        /// Casts the value part of a querystring paramater to a int and returns that value 
        /// </summary>
        /// <param name="name">The name of the querystring param</param>
        /// <returns>Returns a valid int value or null if the parameter did not exist in the querystring</returns>
        protected virtual int? GetQuerystringParam_int(string name)
        {
            HttpContext context = HttpContext.Current;

            if (context.Request.QueryString[name] == null)
                return null;
            return int.Parse(context.Request.QueryString[name].ToString());
        }


        /// <summary>
        /// Casts the value part of a querystring paramater to a bool and returns that value 
        /// </summary>
        /// <param name="name">The name of the querystring param</param>
        /// <returns>Returns a valid bool value or null if the parameter did not exist in the querystring</returns>
        protected virtual bool? GetQuerystringParam_bool(string name)
        {
            HttpContext context = HttpContext.Current;

            if (context.Request.QueryString[name] == null)
                return null;
            return bool.Parse(context.Request.QueryString[name].ToString());
        }

   
        ///// <summary>
        ///// Uses reflection to go through all the object's properties checking if they have been tagged with the "QSPropertyAttribute" attribute
        ///// in which case they are properties that should be included in a querystring.
        ///// </summary>
        ///// <returns>A querystring representing all the stringly type properties of the QueryManager object</returns>
        //public string BuildQuerystring()
        //{
        //    StringBuilder retVal = new StringBuilder();
            
        //    QSPropertyAttribute qsattribute;
        //    foreach (PropertyInfo prop in this.GetType().GetProperties())
        //    {
        //        foreach (Attribute attr in prop.GetCustomAttributes(true))
        //        {
        //            qsattribute = attr as QSPropertyAttribute;
        //            if (null != qsattribute)
        //            {
        //                object val = prop.GetValue(this, null);
        //                if (val !=null)
        //                    {
        //                        retVal.Append(QSDelimiter(retVal.ToString()));
        //                        retVal.Append(qsattribute.QuerystringVarName + "=" + val.ToString());
        //                    }
        //                else if (val == null && qsattribute.ShowIfNull == true)
        //                    {

        //                        retVal.Append(QSDelimiter(retVal.ToString()));
        //                        retVal.Append(qsattribute.QuerystringVarName + "=");
        //                    }
        //            }
        //        }
        //    }

        //    return retVal.ToString();
        //}

        ///// <summary>
        ///// Used when building a querystring for a URL. If the current string is empty then we need to add "?" else the standard "&" querystring delimiter
        ///// </summary>
        ///// <param name="current">The current querystring we're building</param>
        ///// <returns></returns>
        //private string QSDelimiter(string current)
        //{            
        //    if (current==string.Empty)
        //        return "?";
        //    else
        //        return "&";
        //}

    }
}
