﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace SAWD.WebManagers.ScriptManager
{
    internal class CacheManager : CacheManagerBase
    {
        public static string GetCachedScript(string name, HttpContext context) {
            object retVal = GetCache(name, null, context);
            if (retVal != null)
                return retVal.ToString();
            else
                return null;
            

        }

        public static void SetCachedScript(string name, string value, string[] files, HttpContext context) {
            if (files.Length == 0) {
                SetCache(name, value, TimeSpan.FromDays(30));
            }
            else
                SetCacheFileDepend(name, value, files, context);
        }
    }
}
