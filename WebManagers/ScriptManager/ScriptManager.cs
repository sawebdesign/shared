﻿using System;
using System.Web;
using System.Configuration;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Net;

namespace SAWD.WebManagers.ScriptManager
{
    /// <summary>
    /// An HTTPHandler that receives a QueryString parameter called "scriptgroup". This value is used to return a group of .js script files defined in the web.config that will be written to the response.
    /// A typical example of the usage of this would be:
    /// 
    /// <example>
    /// 
    /// <script src="/scripts/scriptmanager.js?scriptgroup=juicewcf.all" type="text/javascript"></script>
    /// </example>
    /// In order to use this handler you have to add the following to your web.config handlers section
    /// <code>
    /// 
    ///   <system.webServer>
    ///   <handlers>
    ///   <add name="scriptmanagerjs" verb="*" path="/scripts/scriptmanager.js"
    ///       type=" SAWD.WebManagers.ScriptManager.Handler, SAWD.WebManagers" preCondition="integratedMode"  />
    ///   </handlers>    
    ///   </system.webServer>    
    /// </code>
    /// You also have to have a section as follows in the web.config
    /// <![CDATA[
    ///   <SAWDWebManagers>
    ///   <ScriptGroups>
    ///     <add name="juicewcf.all">
    ///       <files>
    ///         <add path="/scripts/jquery/jquery.js"/>
    ///         <add path="/scripts/*.js"/>         
    ///         <add type="service" path="/JuiceWCF/Securityservice.svc/js"/>  
    ///       </files>
    ///     </add>
    ///     </ScriptGroups>
    ///   </SAWDWebManagers>
    /// ]]>
    /// </summary>
    public class Handler : IHttpHandler
    {
        /// <summary>
        /// You will need to configure this handler in the web.config file of your 
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        public bool IsReusable {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return true; }
        }

        /// <summary>
        /// ProcessRequest method that receives a QueryString parameter called "scriptgroup". This value is used to return a group of .js script files defined in the web.config that will be written to the response.
        /// </summary>
        /// <param name="context">The curernt HTTPContext</param>
        public void ProcessRequest(HttpContext context) {
            string scriptgroup = context.Request["scriptgroup"];            
            if (scriptgroup == null)
                return;
            string cachename = "scriptgroup_" + scriptgroup;
            string cachedscript = CacheManager.GetCachedScript(cachename, context);
            if (cachedscript == null) {
                WebManagerSettings settings = (WebManagerSettings)ConfigurationManager.GetSection("SAWDWebManagers");
                if (settings.ScriptGroups[scriptgroup] != null) {
                    StringBuilder sb = new StringBuilder();
                    List<string> mainfilelist = new List<string>();
                    //find all the files matching the files collection and populate the cache
                    foreach (ScriptFileElement item in settings.ScriptGroups[scriptgroup].Files) {
                        if (item.Type == "service") {
                            //this is a web service path so we will execute the path locally and write out the script
                            WebRequest request = HttpWebRequest.Create(Utilities.ResolveServerUrl(item.Path, false));
                            WebResponse response = request.GetResponse();
                            StreamReader responseStream = new StreamReader(response.GetResponseStream());
                            sb.AppendLine(responseStream.ReadToEnd());
                            responseStream.Close(); 
                            responseStream.Dispose();
                            response.Close();                            
                        }
                        else {
                            string path = Path.GetDirectoryName(item.Path);
                            string[] files = Directory.GetFiles(context.Server.MapPath(path), Path.GetFileName(item.Path));
                            //read all files matching the path given in the config and read the contents
                            foreach (string filepath in files) {
                                using (FileStream filestream = new FileStream(filepath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)) {
                                    using (System.IO.StreamReader file = new System.IO.StreamReader(filestream)) {
                                        sb.AppendLine(file.ReadToEnd());
                                    }
                                }
                                mainfilelist.Add(filepath);
                            }
                        }                        
                    }
                    //add the file contents to the cache with a dependency based on all the files                    
                    CacheManager.SetCachedScript(cachename, sb.ToString(), mainfilelist.ToArray(), context);
                    cachedscript = CacheManager.GetCachedScript(cachename, context);
                }
                else
                    return;
            }
            context.Response.Write(cachedscript);
            return;
            

        }

        #endregion
    }
}
