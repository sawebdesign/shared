﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SAWD.WebManagers.ScriptManager
{
    [ConfigurationCollection(typeof(ScriptFileElement))]
    public class ScriptFileCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement() {

            return new ScriptFileElement();

        }


        protected override object GetElementKey(ConfigurationElement element) {
            return ((ScriptFileElement)(element)).Path;
        }



        public ScriptFileElement this[int idx] {
            get {
                return (ScriptFileElement)BaseGet(idx);
            }
        }


        public ScriptFileElement this[string name] {
            get {
                return (ScriptFileElement)BaseGet(name);
            }
        }

    }
}
