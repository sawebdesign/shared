﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SAWD.WebManagers.ScriptManager
{
    [ConfigurationCollection(typeof(ScriptGroupElement))]
    public class ScriptGroupCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement() {
            return new ScriptGroupElement();
        }



        protected override object GetElementKey(ConfigurationElement element) {
            return ((ScriptGroupElement)(element)).Name;
        }



        public ScriptGroupElement this[int idx] {
            get {
                return (ScriptGroupElement)BaseGet(idx);
            }
        }


        public ScriptGroupElement this[string name] {
            get {
                return (ScriptGroupElement)BaseGet(name);
            }
        }

    }
}
