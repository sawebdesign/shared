﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using SAWD.WebManagers.ScriptManager;

namespace SAWD.WebManagers
{
    /// <summary>
    /// A class that represents web.config settings for the WebManagers classes. The typical section will look as follows:
    /// <![CDATA[
    ///   <SAWDWebManagers>
    ///   <ScriptGroups>
    ///     <add name="juice.master">
    ///       <files>
    ///         <add path="/scripts/jquery/jquery.js"/>
    ///         <add path="/scripts/jquery/jquery-ui.custom.js"/>        
    ///       </files>
    ///     </add>
    ///     </ScriptGroups>
    ///   </SAWDWebManagers>
    /// ]]>
    /// </summary>
    public class WebManagerSettings : ConfigurationSection
    {
       
        /// <summary>
        /// Collection of ScriptGroups representing grouped scripts to be rendered via one URL
        /// </summary>
        [ConfigurationProperty("ScriptGroups")]
        public ScriptGroupCollection ScriptGroups {
            get { return ((ScriptGroupCollection)(base["ScriptGroups"])); }
        }

    }

}
