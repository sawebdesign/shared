﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SAWD.WebManagers.ScriptManager
{
    /// <summary>
    /// A class representing a file entry in the following web.config section:
    /// <![CDATA[
    ///   <SAWDWebManagers>
    ///   <ScriptGroups>
    ///     <add name="juice.master">
    ///       <files>
    ///         <add path="/scripts/jquery/jquery.js"/>
    ///         <add path="/scripts/*.js"/>         
    ///         <add type="service" path="/JuiceWCF/Securityservice.svc/js"/>  
    ///       </files>
    ///     </add>
    ///     </ScriptGroups>
    ///   </SAWDWebManagers>
    /// ]]>
    /// </summary>
    public class ScriptFileElement : ConfigurationElement 
    {

        /// <summary>
        /// The path to the file. This can be a single file or a wildcard path such as /script/*.js
        /// </summary>
        [ConfigurationProperty("path", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string Path {
            get {
                return ((string)(base["path"]));
            }
            set {
                base["path"] = value;
            }
        }

        /// <summary>
        /// Currently there are only 2 options you can supply. An empty string or "service". "service" is used when the path points to the javascript produced by an endpoint of a WCF - also use this for files that on another server
        /// </summary>
        [ConfigurationProperty("type", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string Type {
            get {
                return ((string)(base["type"]));
            }
            set {
                base["type"] = value;
            }
        }
    }
}
