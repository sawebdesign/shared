﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SAWD.WebManagers.ScriptManager
{
    /// <summary>
    /// Class used to represent the web.config settings for the ScriptManager
    /// Settings will be put into web.config in the following format
    /// <![CDATA[
    ///   <SAWDWebManagers>
    ///   <ScriptGroups>
    ///     <add name="juice.master">
    ///       <files>
    ///         <add path="/scripts/jquery/jquery.js"/>
    ///         <add path="/scripts/*.js"/>         
    ///         <add type="service" path="/JuiceWCF/Securityservice.svc/js"/>  
    ///       </files>
    ///     </add>
    ///     </ScriptGroups>
    ///   </SAWDWebManagers>
    /// ]]>
    /// </summary>
    public class ScriptGroupElement : ConfigurationElement
    {
        [ConfigurationProperty("name", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string Name {
            get {
                return ((string)(base["name"]));
            }
            set {
                base["name"] = value;
            }
        }

        /// <summary>
        /// A collection of paths to files that will be included in the generated script
        /// </summary>
        [ConfigurationProperty("files")]
        public ScriptFileCollection Files {
            get { return ((ScriptFileCollection)(base["files"])); }
        }
    }
}
