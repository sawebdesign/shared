﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SAWD.ExceptionHandler
{
    public class ExceptionHandlerSettings : ConfigurationSection
    {


        //[ConfigurationProperty("ExceptionHandlers")]
        //public ExceptionHandlerElementCollection ExceptionHandler {
        //    get { return ((ExceptionHandlerElementCollection)(base["ExceptionHandlers"])); }
        //}

        [ConfigurationProperty("name", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string Name {
            get {
                return ((string)(base["name"]));
            }
            set {
                base["name"] = value;
            }
        }

        [ConfigurationProperty("errorlogpath", DefaultValue = "", IsKey = false, IsRequired = true)]
        public string ErrorLogPath {
            get {
                return ((string)(base["errorlogpath"]));
            }
            set {
                base["errorlogpath"] = value;
            }
        }

        [ConfigurationProperty("resourcehelper", IsKey = false, IsRequired = false)]
        public string ResourceHelper {
            get {
                return ((string)(base["resourcehelper"]));
            }
            set {
                base["resourcehelper"] = value;
            }
        }

        [ConfigurationProperty("servererrormessage", DefaultValue = "The server encountered an error.", IsKey = false, IsRequired = false)]
        public string ServerErrorMessage {
            get {
                return ((string)(base["servererrormessage"]));
            }
            set {
                base["servererrormessage"] = value;
            }
        }

        [ConfigurationProperty("servererrorwithreference", DefaultValue = "The server encountered an error. Please contact the administrator with the following reference ({0}).", IsKey = false, IsRequired = false)]
        public string ServerErrorWithReference {
            get {
                return ((string)(base["servererrorwithreference"]));
            }
            set {
                base["servererrorwithreference"] = value;
            }
        }


        [ConfigurationProperty("logerrorstofile", DefaultValue = false, IsKey = false, IsRequired = true)]
        public bool LogErrorsToFile {
            get {
                return ((bool)(base["logerrorstofile"]));
            }
            set {
                base["logerrorstofile"] = value;
            }
        }

        [ConfigurationProperty("errorcodestolog")]
        public LogFileElementCollection ErrorCodesToLog {
            get { return ((LogFileElementCollection)(base["errorcodestolog"])); }
        }

        [ConfigurationProperty("errorcodestosuppress")]
        public LogFileElementCollection ErrorCodesToSuppress {
            get { return ((LogFileElementCollection)(base["errorcodestosuppress"])); }
        }
    }

}
