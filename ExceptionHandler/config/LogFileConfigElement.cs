﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SAWD.ExceptionHandler
{
    public class LogFileConfigElement : ConfigurationElement 
    {
        [ConfigurationProperty("name", DefaultValue = "Other", IsKey = true, IsRequired = true)]
        public FaultCodes Name {
            get {
                return ((FaultCodes)(base["name"]));
            }
            set {
                base["name"] = value;
            }
        }
    }
}
