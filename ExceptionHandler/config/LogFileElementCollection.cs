﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SAWD.ExceptionHandler
{
    [ConfigurationCollection(typeof(LogFileConfigElement))]
    public class LogFileElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement() {
            return new LogFileConfigElement();
        }

        protected override object GetElementKey(ConfigurationElement element) {
            return ((LogFileConfigElement)(element)).Name;
        }

        public LogFileConfigElement this[int idx] {
            get { return (LogFileConfigElement)BaseGet(idx); }
            set { if (BaseGet(idx) != null) { BaseRemoveAt(idx); } BaseAdd(idx, value); }
        }

        public LogFileConfigElement this[string Name] {
            get {
                for (int i = 0; i < this.Count; i++) {
                    if (this[i].Name.ToString() == Name)
                        return this[i];
                }
                return null;
            }
        }

    }
}
