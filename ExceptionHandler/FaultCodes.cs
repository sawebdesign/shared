﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SAWD.ExceptionHandler
{
    /// <summary>
    /// An enum used for keeping a standard list of fault codes handled by the WCF service. This is used so that we have one spot where a name is used
    /// </summary>
    public enum FaultCodes
    {
        /// <summary>
        /// User didn't have sufficient security rights
        /// </summary>
        Security = 1,
  
        /// <summary>
        /// Database connection errors
        /// </summary>
        Database = 2,  

        /// <summary>
        /// The paramters passed to the WCF are not the same type as the WCF expects
        /// </summary>
        Parameters = 3,

        /// <summary>
        /// When WCF converts an unhandled exception to a FaulException it defines this as a FaultCode
        /// </summary>
        Sender = 4,      

        /// <summary>
        /// General errors
        /// </summary>
        Other = 5,     

        /// <summary>
        /// Used for warning when validation rules are broken - this is not the same as when we are explicitly validating data. Only to be used when an IEntity object has broken validation rules
        /// </summary>
        ValidationRules = 6,    //

        /// <summary>
        /// Used for warning when no data has changed
        /// </summary>
        DataUnchanged = 7,

        /// <summary>
        /// Used for warning that the data being overwritten is newer than the source data
        /// </summary>
        DataExpired = 8,
     
        /// <summary>
        /// Used when there is an error with explicit validation of data anywhere in the site
        /// </summary>
        DataValidation = 9
    }
}
