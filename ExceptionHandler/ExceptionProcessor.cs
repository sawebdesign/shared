﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.ServiceModel;
using System.Diagnostics;
using System.Runtime.Serialization.Json;
using System.IO;

namespace SAWD.ExceptionHandler
{


    /// <summary>
    /// Class with various methods used for processing/handling exceptions
    /// </summary>
    public class ExceptionProcessor
    {
        /// <summary>
        /// Method that logs exceptions to a file names with a GUID
        /// Exceptions are only logged if the config file is set to log errors and if the error type is listed in the codes to log
        /// </summary>
        /// <param name="ex">The exception that was thrown</param>
        /// <param name="ajaxerror">The AjaxError object that has been created to send back to the client</param>
        /// <returns>The GUID which is the name of the error log file</returns>
        internal static string LogException(Exception ex, WCFReturnMessage sawdexception) {
            string retVal = Guid.NewGuid().ToString();
            ExceptionHandlerSettings settings = (ExceptionHandlerSettings)ConfigurationManager.GetSection("SAWDErrorHandler");
            string path = System.Web.HttpContext.Current.Server.MapPath(settings.ErrorLogPath);

            if (settings.LogErrorsToFile) {
                if (settings.ErrorCodesToLog[sawdexception.FaultCode] == null) {
                    return "";
                }

                //check if the path exists
                if (!Directory.Exists(path)) {
                    Directory.CreateDirectory(path);
                }

                //we will create a file for each error created. The file will be given the guid as a name
                System.IO.StreamWriter sr = System.IO.File.CreateText(path + @"\" + retVal);
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Date/Time: " + DateTime.Now.ToString());
                sb.AppendLine("Description: " + ex.Message);
                sb.AppendLine("Stack trace: " + ex.StackTrace);
                sb.AppendLine("Source: " + ex.Source);
                if (ex.InnerException != null) {
                    System.Diagnostics.Trace.WriteLine("Inner exception: " + ex.InnerException.Message);
                    System.Diagnostics.Trace.WriteLine("Inner exception stack trace: " + ex.InnerException.StackTrace);
                }
                sr.Write(sb.ToString());
                sr.Close();
                return retVal;
            }
            return "";
        }

        /// <summary>
        /// Method used for creating a SAWDException object which is used both by the Web application and the WCF application to relay user friendly messages to the client
        /// </summary>
        /// <param name="exception">The exception that occurred</param>
        /// <returns>A SAWDException object representing the error that occurred, but with friendly messages if needed</returns>
        public static WCFReturnMessage ProcessException(Exception exception) {
            bool useInnerException = false;
            ExceptionHandlerSettings settings = (ExceptionHandlerSettings)ConfigurationManager.GetSection("SAWDErrorHandler");

            WCFReturnMessage wcfreturnmessage = new WCFReturnMessage();
            //write all exceptions to the Trace
            if (exception.InnerException != null) {
                System.Diagnostics.Trace.WriteLine("Inner exception: " + exception.InnerException.Message);
                System.Diagnostics.Trace.WriteLine("Inner exception stack trace: " + exception.InnerException.StackTrace);
            } 
            

            //check what Culture is currently set and set that for handling the exception
            if (SessionManager.CultureName!=null)
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(SessionManager.CultureName);

            //if this is a WarningFaultException then it could contain "ReturnObjects"
            if (exception.GetType() == typeof(WarningFaultException) || exception.GetType().BaseType == typeof(WarningFaultException)) {
                foreach (var item in ((WarningFaultException)(exception)).ReturnObjects) {
                    //TODO: Pedro - Need to look into this serialization because right now if the error was a serialization error then we create an indefinite recursion
                    //Similarly, if the "item" can't be serilized but we don't know that yet i.e. a dodgy date this also results in  an indefinite recursion
                    wcfreturnmessage.DataObjects.Add(SerializeJSON(item));
                }                
            }

            if (exception.GetType() == typeof(System.ServiceModel.FaultException) || exception.GetType().BaseType == typeof(System.ServiceModel.FaultException)) {
                wcfreturnmessage.FaultCode = ((FaultException)(exception)).Code.Name;
            } else if (exception.InnerException != null) {
                //if this has an inner exception, check if it is a WCF exception
                if (exception.InnerException.GetType() == typeof(System.ServiceModel.FaultException) || exception.InnerException.GetType().BaseType == typeof(System.ServiceModel.FaultException)) {
                    wcfreturnmessage.FaultCode = ((FaultException)(exception.InnerException)).Code.Name;
                    useInnerException = true;
                } else {
                    wcfreturnmessage.FaultCode = SAWD.ExceptionHandler.FaultCodes.Other.ToString();
                }
            } else if (exception.GetType() == typeof(System.Data.SqlClient.SqlException)) {
                wcfreturnmessage.FaultCode = SAWD.ExceptionHandler.FaultCodes.Database.ToString();
            } else {
                wcfreturnmessage.FaultCode = SAWD.ExceptionHandler.FaultCodes.Other.ToString();
            }
            wcfreturnmessage.Reference = ExceptionProcessor.LogException(exception, wcfreturnmessage);

            //Set the error messages
            if (useInnerException) {
                wcfreturnmessage.ErrorMessage = exception.InnerException.Message;
            } else {
                wcfreturnmessage.ErrorMessage = exception.Message;
            }

            //Some error messages should be suppressed and a general message sent to the client so overwrite the original message with friendly
            if (settings.ErrorCodesToSuppress.Count > 0) {
                if (settings.ErrorCodesToSuppress[wcfreturnmessage.FaultCode] != null) {
                    return wcfreturnmessage;
                }

                //try load the ResourceHelper in the settings if it doesn't exist load the values from the settings
                string servererror = string.Empty;
                string servererrorwithref = string.Empty;
                if (settings.ResourceHelper == null || settings.ResourceHelper == string.Empty) {
                    servererror = settings.ServerErrorMessage;
                    servererrorwithref = settings.ServerErrorWithReference;
                } else {
                    //we know that all ResourceHelpers must implement the SAWDResourceHelper.ISysResources interface
                    //therefore we can happily cast the dynamically loaded library to that Interface and call the 2 required methods
                    SAWD.ResourceHelper.ISysResources sysResources = (SAWD.ResourceHelper.ISysResources)(Activator.CreateInstance(settings.ResourceHelper.Split(',')[1], settings.ResourceHelper.Split(',')[0]).Unwrap());
                    servererror = sysResources.GetNonStaticLabel("Labels", "ServerError");
                    servererrorwithref = sysResources.GetNonStaticLabel("Labels", "ServerErrorWithReference");
                }
                if (wcfreturnmessage.Reference != "") {
                    wcfreturnmessage.ErrorMessage = string.Format(servererrorwithref, wcfreturnmessage.Reference);
                } else {
                    wcfreturnmessage.ErrorMessage = servererror;
                }
            }

            return wcfreturnmessage;
        }

        /// <summary>
        /// Private method used to serialize objects to JSON
        /// </summary>
        /// <param name="obj">Any object</param>
        /// <returns>A JSON serialization of obj</returns>
        private static string SerializeJSON(object obj) {           
            string retVal = string.Empty;

            MemoryStream ms = new MemoryStream();
            var json = new DataContractJsonSerializer(obj.GetType());
            json.WriteObject(ms, obj);
            
            retVal = Encoding.UTF8.GetString(ms.ToArray());
            ms.Close();
            ms.Dispose();

            return retVal;
        }
    }
}
