﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;

namespace SAWD.ExceptionHandler
{
    /// <summary>
    /// A FaultException used for warning messages to be sent back by the WCF methods. It has an extra property called ReturnObjects which holds a list of Objects that we need to eventually serialize and send back to the client
    /// </summary>
    public class WarningFaultException : FaultException 
    {
        public WarningFaultException(FaultReason faultreason, FaultCode faultcode)
            : base(faultreason, faultcode) {
                
        }

        protected WarningFaultException(SerializationInfo info, StreamingContext context)  {
            //if (info != null)
            //    this.ErrorMessage = info.GetString("ErrorMessage");
        }


       // public object MyProperty { get; set; }

        private List<object> returnobjects = new List<object>();

        public List<object> ReturnObjects {
            get { return this.returnobjects; }
            set { this.returnobjects = value; }
        }
    }
}
