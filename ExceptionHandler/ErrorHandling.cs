﻿using System;
using System.ServiceModel.Dispatcher;
using System.Runtime.Serialization;
using System.ServiceModel.Web;
using System.ServiceModel.Channels;
using System.Runtime.Serialization.Json;
using System.Net;
using System.ServiceModel.Description;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Collections;
using System.Collections.Generic;

namespace SAWD.ExceptionHandler
{
    /// <summary>
    /// A class that implements IErrorHandler in order to capture WCF error messages and send meaningful messages down to the client
    /// This can be used for other logic such as error logging and hiding specific messages from the client e.g. connection strings etc.
    /// Note that the overriden ProvideFault method is where the bulk of the customisation takes place
    /// </summary>
    public class ExceptionHandlerEx : IErrorHandler
    {
        public bool HandleError(Exception error) {
            return true;
        }

        /// <summary>
        /// Captures all errors and sends back an OK status, as well as a valid JSON object representing the error
        /// </summary>
        /// <param name="exception">The exception raised</param>
        /// <param name="version"></param>
        /// <param name="message">The message to send back to the client</param>
        public void ProvideFault(Exception exception, MessageVersion version, ref Message message) {
            WCFReturnMessage wcfreturnmessage = ExceptionProcessor.ProcessException(exception);

            message = Message.CreateMessage(version, "", wcfreturnmessage, new DataContractJsonSerializer(wcfreturnmessage.GetType()));
            var wbf = new WebBodyFormatMessageProperty(WebContentFormat.Json);
            message.Properties.Add(WebBodyFormatMessageProperty.Name, wbf);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json";
            WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;
            //WebOperationContext.Current.OutgoingResponse.StatusDescription = exception.Message;
            WebOperationContext.Current.OutgoingResponse.StatusDescription = "Error occurred, please check attached message";
        }
    }

    /// <summary>
    /// Represents an error message and can be serialized as a JSON object
    /// </summary>
    [DataContract]
    public class WCFReturnMessage
    {
        public WCFReturnMessage() {
            this.DataObjects = new List<string>();
        }

        public WCFReturnMessage(string message, string faultcode, string reference, List<string> dataobjects) {
            this.ErrorMessage = message;
            this.FaultCode = faultcode;
            this.Reference = reference;
            this.DataObjects = dataobjects;
        }

        [DataMember]
        public String ErrorMessage { get; set; }
        [DataMember]
        public String FaultCode { get; set; }
        [DataMember]
        public String Reference { get; set; }
        [DataMember]
        public List<string> DataObjects { get; set; }
    }

}