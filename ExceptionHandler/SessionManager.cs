﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SAWD.ExceptionHandler
{
    public class SessionManager : SAWD.WebManagers.SessionManagerBase
    {

        /// <summary>
        /// This is the culture that the user has i.e. en-ZA
        /// </summary>        
        public static string CultureName {
            get {
                if (GetSession("CultureName") != null)
                    return GetSession("CultureName").ToString();
                else
                    return null;
            }
            set { SetSession("CultureName", value); }
        }

    }
}
