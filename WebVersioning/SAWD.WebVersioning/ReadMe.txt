﻿Author:		Gerard Matthews
Date:		2014/11/18

Add the existing SAWD.WebVersioning project to your solution (in your global solution folder or wherever you like)
Add a reference to the SAWD.WebVersioning project to your web project
Add a Global.asax to your website.

Overwrite/Add the following function.

protected void Application_BeginRequest(object sender, EventArgs e) {
	if(Request.IsLocal)
		System.Threading.Thread.Sleep(50);

	var request = Request;
	string currentRelativePath = request.AppRelativeCurrentExecutionFilePath;

	if(request.HttpMethod == "GET") {
		if(currentRelativePath.EndsWith(".aspx")) {
			var folderPath = currentRelativePath.Substring(2, currentRelativePath.LastIndexOf('/') - 1);
			Response.Filter = new SAWD.WebVersioning.JSVersion(Response, relativePath => {
				if(Context.Cache[relativePath] == null) {
					try {
						var physicalPath = Server.MapPath(relativePath);
						var version = "?v=" + new System.IO.FileInfo(physicalPath).LastWriteTime.ToString("yyyyMMddhhmmss");
						Context.Cache.Add(relativePath, version, null, DateTime.Now.AddMinutes(1), TimeSpan.Zero, CacheItemPriority.Normal, null);
						return version;
					} catch {
						return "";
					}

				} else {
					return Context.Cache[relativePath] as string;
				}
			});
		}
	}
}