﻿using HtmlAgilityPack;
using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace SAWD.WebVersioning
{
	public class JSVersion : Stream {

		#region Fields

		private static readonly char[] htmlEnd = "</html>".ToCharArray();
		Encoding _Encoding;

		/// <summary>
		/// Holds characters from last Write(...) call where the start tag did not
		/// end and thus the remaining characters need to be preserved in a buffer so 
		/// that a complete tag can be parsed
		/// </summary>
		private char[] _PendingBuffer = null;
		private Stream _ResponseStream;
		//private StringBuilder debug = new StringBuilder();
		private Func<string, string> _getVersionOfFile;

		#endregion Fields

		#region Constructors

		public JSVersion(HttpResponse response, Func<string, string> getVersionOfFile) {
			this._Encoding = response.Output.Encoding;
			this._ResponseStream = response.Filter;
			this._getVersionOfFile = getVersionOfFile;
		}

		#endregion Constructors

		#region Properties

		public override bool CanRead {
			get { return false; }
		}

		public override bool CanSeek {
			get { return false; }
		}

		public override bool CanWrite {
			get { return true; }
		}

		public override long Length {
			get { return 0; }
		}

		public override long Position { get; set; }

		#endregion Properties

		#region Methods

		public override void Close() {
			this.FlushPendingBuffer();
			_ResponseStream.Close();
			_ResponseStream = null;
			_getVersionOfFile = null;
			_PendingBuffer = null;
		}

		public override void Flush() {
			this.FlushPendingBuffer();
			_ResponseStream.Flush();
		}

		public override int Read(byte[] buffer, int offset, int count) {
			return _ResponseStream.Read(buffer, offset, count);
		}

		public override long Seek(long offset, SeekOrigin origin) {
			return _ResponseStream.Seek(offset, origin);
		}

		public override void SetLength(long length) {
			_ResponseStream.SetLength(length);
		}

		public override void Write(byte[] buffer, int offset, int count) {
			char[] content;
			char[] charBuffer = this._Encoding.GetChars(buffer, offset, count);

			/// If some bytes were left for processing during last Write call
			/// then consider those into the current buffer
			if(null != this._PendingBuffer) {
				content = new char[charBuffer.Length + this._PendingBuffer.Length];
				Array.Copy(this._PendingBuffer, 0, content, 0, this._PendingBuffer.Length);
				Array.Copy(charBuffer, 0, content, this._PendingBuffer.Length, charBuffer.Length);
				this._PendingBuffer = null;
			} else {
				content = charBuffer;
			}

			string tmphtml = new string(content);
			if(!tmphtml.Contains("</html>")) {
				this._PendingBuffer = new char[content.Length];
				Array.Copy(content, 0, this._PendingBuffer, 0, content.Length);
				return;
			}

			HtmlDocument doc = new HtmlDocument();
			doc.LoadHtml(new string(content));
			HtmlNodeCollection scriptTag = doc.DocumentNode.SelectNodes("//script[@src]");
			if(scriptTag != null) {
				foreach(HtmlNode node in scriptTag) {
					if(!string.IsNullOrEmpty(node.Attributes["src"].Value)) {
						node.SetAttributeValue("src", node.Attributes["src"].Value + this._getVersionOfFile(node.Attributes["src"].Value));
					}
				}
			}

			HtmlNodeCollection cssTag = doc.DocumentNode.SelectNodes("//link[@href]");
			if(cssTag != null) {
				foreach(HtmlNode node in cssTag) {
					if(!string.IsNullOrEmpty(node.Attributes["href"].Value)) {
						node.SetAttributeValue("href", node.Attributes["href"].Value + this._getVersionOfFile(node.Attributes["href"].Value));
					}
				}
			}

			content = doc.DocumentNode.OuterHtml.ToCharArray();

			this.WriteOutput(content, 0, content.Length);
		}

		private void FlushPendingBuffer() {
			/// Some characters were left in the buffer
			if(null != this._PendingBuffer) {
				this.WriteOutput(this._PendingBuffer, 0, this._PendingBuffer.Length);
				this._PendingBuffer = null;
			}
		}

		private void WriteBytes(byte[] bytes, int pos, int length) {
			this._ResponseStream.Write(bytes, 0, bytes.Length);
		}

		private void WriteOutput(char[] content, int pos, int length) {
			if(length == 0)
				return;

			//debug.Append(content, pos, length);
			byte[] buffer = this._Encoding.GetBytes(content, pos, length);
			this.WriteBytes(buffer, 0, buffer.Length);
		}

		#endregion Methods

	}

}
