﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SAWD.ResourceHelper
{
    public interface ISysResources
    {
        string GetNonStaticFriendlyName(string name);
        string GetNonStaticLabel(string resourcetype, string name);
    }
}
