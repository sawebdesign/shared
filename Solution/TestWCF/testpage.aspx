﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="testpage.aspx.cs" Inherits="TestWCF.testpage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <script src="/scripts/MicrosoftAjax.debug.js" type="text/javascript"></script>
    <!--<script src="/TestService.svc/jsdebug" type="text/javascript"></script>-->
    <script src="scripts/testservice.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">

        function DoRequest() {
            var myobj = new Object();
            myobj.SysUserId = "123";
            myobj.UserName = "myname";
            myobj.Password = "password";
            JuiceWCF.Security.Service1.GetUser(myobj, HandleRequest, Failed);
            return false;
        }

        function Failed(result) {

        }

        function HandleRequest(result) {

        }
    </script>

    <form id="form1" runat="server">
    <div>
        <button onclick="return DoRequest();">Test service</button>
    </div>
    </form>
</body>
</html>
