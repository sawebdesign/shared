﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.ServiceModel.Activation;
using SAWD.ExceptionHandler;

namespace TestWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceContract(Namespace = "JuiceWCF.Security")]
    public class Service1
    {
         [OperationContract]   
        public string GetData(int value) {
            WarningFaultException warning = new WarningFaultException(new FaultReason("This is a reason"), new FaultCode(SAWD.ExceptionHandler.FaultCodes.Security.ToString()));
           // warning.ReturnObjects.Add(value);
            throw warning;

            return string.Format("You entered: {0}", value);
        }

         [OperationContract]
         public TestWCF.TestClasses.SysUserTest GetUser(TestWCF.TestClasses.SysUserTest value) {
            // WarningFaultException warning = new WarningFaultException(new FaultReason("This is a reason"), new FaultCode(SAWD.ExceptionHandler.FaultCodes.Security.ToString()));
            // warning.ReturnObjects.Add(value);
             
            //// warning.MyProperty = value;
            // throw warning;

             SessionManager.CultureName = "123456789";

             //int i = int.Parse("sdfsdfs");

             return value;
         }


    }
}
