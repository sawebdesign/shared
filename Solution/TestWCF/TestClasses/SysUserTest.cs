﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace TestWCF.TestClasses
{
    [Serializable]
    [DataContract]
    public class SysUserTest
    {
        [DataMember]
        public int SysUserId { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string Password { get; set; }
    }
}