﻿Type.registerNamespace('JuiceWCF.Security');
JuiceWCF.Security.Service1 = function () {
    JuiceWCF.Security.Service1.initializeBase(this);
    this._timeout = 0;
    this._userContext = null;
    this._succeeded = null;
    this._failed = null;
}
JuiceWCF.Security.Service1.prototype = {
    _get_path: function () {
        var p = this.get_path();
        if (p) return p;
        else return JuiceWCF.Security.Service1._staticInstance.get_path();
    },
    GetData: function (value, succeededCallback, failedCallback, userContext) {
        /// <param name="value" type="Number">System.Int32</param>
        /// <param name="succeededCallback" type="Function" optional="true" mayBeNull="true"></param>
        /// <param name="failedCallback" type="Function" optional="true" mayBeNull="true"></param>
        /// <param name="userContext" optional="true" mayBeNull="true"></param>
        return this._invoke(this._get_path(), 'GetData', false, { value: value }, succeededCallback, failedCallback, userContext);
    },
    GetUser: function (value, succeededCallback, failedCallback, userContext) {
        /// <param name="value" type="TestWCF.TestClasses.SysUserTest">TestWCF.TestClasses.SysUserTest</param>
        /// <param name="succeededCallback" type="Function" optional="true" mayBeNull="true"></param>
        /// <param name="failedCallback" type="Function" optional="true" mayBeNull="true"></param>
        /// <param name="userContext" optional="true" mayBeNull="true"></param>
        return this._invoke(this._get_path(), 'GetUser', false, { value: value }, succeededCallback, failedCallback, userContext);
    } 
}
JuiceWCF.Security.Service1.registerClass('JuiceWCF.Security.Service1', Sys.Net.WebServiceProxy);
JuiceWCF.Security.Service1._staticInstance = new JuiceWCF.Security.Service1();
JuiceWCF.Security.Service1.set_path = function (value) {
    JuiceWCF.Security.Service1._staticInstance.set_path(value);
}
JuiceWCF.Security.Service1.get_path = function () {
    /// <value type="String" mayBeNull="true">The service url.</value>
    return JuiceWCF.Security.Service1._staticInstance.get_path();
}
JuiceWCF.Security.Service1.set_timeout = function (value) {
    JuiceWCF.Security.Service1._staticInstance.set_timeout(value);
}
JuiceWCF.Security.Service1.get_timeout = function () {
    /// <value type="Number">The service timeout.</value>
    return JuiceWCF.Security.Service1._staticInstance.get_timeout();
}
JuiceWCF.Security.Service1.set_defaultUserContext = function (value) {
    JuiceWCF.Security.Service1._staticInstance.set_defaultUserContext(value);
}
JuiceWCF.Security.Service1.get_defaultUserContext = function () {
    /// <value mayBeNull="true">The service default user context.</value>
    return JuiceWCF.Security.Service1._staticInstance.get_defaultUserContext();
}
JuiceWCF.Security.Service1.set_defaultSucceededCallback = function (value) {
    JuiceWCF.Security.Service1._staticInstance.set_defaultSucceededCallback(value);
}
JuiceWCF.Security.Service1.get_defaultSucceededCallback = function () {
    /// <value type="Function" mayBeNull="true">The service default succeeded callback.</value>
    return JuiceWCF.Security.Service1._staticInstance.get_defaultSucceededCallback();
}
JuiceWCF.Security.Service1.set_defaultFailedCallback = function (value) {
    JuiceWCF.Security.Service1._staticInstance.set_defaultFailedCallback(value);
}
JuiceWCF.Security.Service1.get_defaultFailedCallback = function () {
    /// <value type="Function" mayBeNull="true">The service default failed callback.</value>
    return JuiceWCF.Security.Service1._staticInstance.get_defaultFailedCallback();
}
JuiceWCF.Security.Service1.set_enableJsonp = function (value) { JuiceWCF.Security.Service1._staticInstance.set_enableJsonp(value); }
JuiceWCF.Security.Service1.get_enableJsonp = function () {
    /// <value type="Boolean">Specifies whether the service supports JSONP for cross domain calling.</value>
    return JuiceWCF.Security.Service1._staticInstance.get_enableJsonp();
}
JuiceWCF.Security.Service1.set_jsonpCallbackParameter = function (value) { JuiceWCF.Security.Service1._staticInstance.set_jsonpCallbackParameter(value); }
JuiceWCF.Security.Service1.get_jsonpCallbackParameter = function () {
    /// <value type="String">Specifies the parameter name that contains the callback function name for a JSONP request.</value>
    return JuiceWCF.Security.Service1._staticInstance.get_jsonpCallbackParameter();
}
JuiceWCF.Security.Service1.set_path("http://localhost:51192/TestService.svc");
JuiceWCF.Security.Service1.GetData = function (value, onSuccess, onFailed, userContext) {
    /// <param name="value" type="Number">System.Int32</param>
    /// <param name="succeededCallback" type="Function" optional="true" mayBeNull="true"></param>
    /// <param name="failedCallback" type="Function" optional="true" mayBeNull="true"></param>
    /// <param name="userContext" optional="true" mayBeNull="true"></param>
    JuiceWCF.Security.Service1._staticInstance.GetData(value, onSuccess, onFailed, userContext);
}
JuiceWCF.Security.Service1.GetUser = function (value, onSuccess, onFailed, userContext) {
    /// <param name="value" type="TestWCF.TestClasses.SysUserTest">TestWCF.TestClasses.SysUserTest</param>
    /// <param name="succeededCallback" type="Function" optional="true" mayBeNull="true"></param>
    /// <param name="failedCallback" type="Function" optional="true" mayBeNull="true"></param>
    /// <param name="userContext" optional="true" mayBeNull="true"></param>
    JuiceWCF.Security.Service1._staticInstance.GetUser(value, onSuccess, onFailed, userContext);
}
var gtc = Sys.Net.WebServiceProxy._generateTypedConstructor;
Type.registerNamespace('TestWCF.TestClasses');
if (typeof (TestWCF.TestClasses.SysUserTest) === 'undefined') {
    TestWCF.TestClasses.SysUserTest = gtc("SysUserTest:http://schemas.datacontract.org/2004/07/TestWCF.TestClasses");
    TestWCF.TestClasses.SysUserTest.registerClass('TestWCF.TestClasses.SysUserTest');
}
