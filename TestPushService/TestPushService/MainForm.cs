﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.IO;

namespace TestPushService
{
    public partial class MainForm : Form
    {
        public MainForm() {
            InitializeComponent();
        }



        private void btnSpawn_Click(object sender, EventArgs e) {
            btnSpawn.Enabled = false;
            StatCounter.Instance.FirstTimeReturns = 0;
            StatCounter.Instance.SecondTimeReturns = 0;
            StatCounter.Instance.TotalThreads = 0;
            SimulateClientData clientdata = new SimulateClientData();
            populateDataLists(ref clientdata);
            
            Thread t;
            for (int n = 1; n <= Int32.Parse(txtThreadCount.Text); n++) {

                t = new Thread(new ParameterizedThreadStart(simulateClient));
                t.Start(clientdata);
                Console.WriteLine(System.DateTime.Now.TimeOfDay + ": Launched thread " + n);
                //Thread.Sleep(500);
                StatCounter.Instance.TotalThreads++;
            }

            Console.WriteLine("All threads created. Hit enter to view stats.");

            while (Console.ReadLine() != "end") {
                Console.WriteLine("Total threads created:" + StatCounter.Instance.TotalThreads.ToString());
                for (int i = 0; i < clientdata.URLs.Count; i++) {
                    Console.WriteLine("Returns for response no " + i.ToString() + ": " + StatCounter.Instance.ReturnCounter[i].ToString());
                }
                //Console.WriteLine("Total first time returns:" + StatCounter.Instance.FirstTimeReturns.ToString());
                //Console.WriteLine("Total second time returns:" + StatCounter.Instance.SecondTimeReturns.ToString());
                Console.WriteLine("Type 'end' to end these messages.");
                Thread.Sleep(1000);
            }
        }

        private void populateDataLists(ref SimulateClientData data) {
            data.URLs = new List<string>();
            data.Data = new List<string>();

            foreach (string item in txtURL.Lines ) {
                data.URLs.Add(item);
                StatCounter.Instance.ReturnCounter.Add(0);
            }

            foreach (string item in txtData.Lines) {
                data.Data.Add(item);
            }
        }

        private static void simulateClient(object clientdata) {

            for (int i = 0; i < ((SimulateClientData)(clientdata)).URLs.Count; i++) {
                string url = ((SimulateClientData)(clientdata)).URLs[i];
                string data = "";
                if (i < ((SimulateClientData)(clientdata)).Data.Count)
                    data = ((SimulateClientData)(clientdata)).Data[i];
                string returndata = GetServerResponse(url, data);
                
                StatCounter.Instance.ReturnCounter[i]++;
//                StatCounter.Instance.FirstTimeMessages.Add(returndata);
            }
        }

        private static string GetServerResponse(string serviceurl, string data) {
            string retVal = String.Empty;
            // Create a request using a URL that can receive a post. 
            WebRequest request = WebRequest.Create(serviceurl);
            // Set the Method property of the request to POST.
            request.Timeout = 80000;
            request.Method = "POST";
            // Create POST data and convert it to a byte array.
            byte[] byteArray = Encoding.UTF8.GetBytes(data);
            // Set the ContentType property of the WebRequest.
            request.ContentType = "application/json; charset=utf-8";
            // Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length;
            // Get the request stream.
            Stream dataStream = request.GetRequestStream();
            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);
            // Close the Stream object.
            dataStream.Close();
            // Get the response.
            WebResponse response = request.GetResponse();
            // Display the status.
            Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            if (((HttpWebResponse)response).StatusDescription != "OK")
                StatCounter.Instance.ErroredReturns++;
            // Get the stream containing content returned by the server.
            dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            retVal = reader.ReadToEnd();
            reader.Close();
            dataStream.Close();
            response.Close();

            return retVal;
        }

        private void txtData_TextChanged(object sender, EventArgs e) {

        }
    }
}
