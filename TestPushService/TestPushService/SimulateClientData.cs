﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestPushService
{
    public class SimulateClientData
    {
        public List<string> URLs { get; set; }
        public List<string> Data { get; set; }
    }
}
