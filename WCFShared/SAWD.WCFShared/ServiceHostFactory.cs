﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Activation;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.ServiceModel.Dispatcher;
using System.Web;

namespace SAWD.WCFShared
{

    public class CRSyncServiceHost : ServiceHost
    {
        public CRSyncServiceHost(Type serviceType, params Uri[] baseAddresses) : base(serviceType, baseAddresses) { }

        protected override void ApplyConfiguration() {
            base.ApplyConfiguration();
        }
    }


    /// <summary>
    /// Extension of the WebHttpBehavior class
    /// Note the overridden AddServerHandlers method that clears the default error handlers and adds the one we create above
    /// </summary>
    public class WebHttpBehaviorEx : WebHttpBehavior
    {
        /// <summary>
        /// Overridden method that clears all current ErrorHandlers and ads our extend one from above
        /// </summary>
        /// <param name="endpoint"></param>
        /// <param name="endpointDispatcher"></param>
        protected override void AddServerErrorHandlers(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher) {
            endpointDispatcher.ChannelDispatcher.ErrorHandlers.Clear();
            endpointDispatcher.ChannelDispatcher.ErrorHandlers.Add(new SAWD.ExceptionHandler.ExceptionHandlerEx());
        }
    }

    /// <summary>
    /// Extend the WebScriptServiceHostFactory class, which is the default factory for Web enabled WCF
    /// The WebScriptServiceHostFactory is the default factory used for WCF services that will be accessed via AJAX.
    /// Note the overriden CreateServiceHost method which recreates the default behaviour but also adds the WebHttpBehaviorEx behaviour created above.
    /// </summary>
    public class ServiceHostFactory : WebScriptServiceHostFactory
    {
        /// <summary>
        /// Overrides the CreateServiceHost method of WebScriptServiceHostFactory, but still keeps the default behaviour with the addition
        /// of adding the newly created behaviour that handles errors
        /// </summary>
        /// <param name="serviceType"></param>
        /// <param name="baseAddresses"></param>
        /// <returns></returns>
        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses) {
            //the site could potentially have multiple bindings in which case we want to use the baseuri for the request that came through
            CRSyncServiceHost host = new CRSyncServiceHost(serviceType, FilterBaseAddresses(baseAddresses));

            host.AddDefaultEndpoints();
            ServiceEndpoint endpoint;
            if (host.Description.Endpoints.Count > 0)
                endpoint = host.Description.Endpoints[0];
            else {
                endpoint = host.AddServiceEndpoint(serviceType.FullName, new System.ServiceModel.BasicHttpBinding("basicBinding"), baseAddresses[0]);
            }
            System.ServiceModel.Channels.BindingElement bindingElement = new System.ServiceModel.Channels.TcpTransportBindingElement();
            System.ServiceModel.Channels.CustomBinding binding = new System.ServiceModel.Channels.CustomBinding(bindingElement);

            //Add MEX endpoint
            host.AddServiceEndpoint(serviceType.FullName, MetadataExchangeBindings.CreateMexHttpBinding(), "mex");

            //Add SOAP endpoint
            host.AddServiceEndpoint(serviceType.FullName, new System.ServiceModel.BasicHttpBinding("basicBinding"), baseAddresses[0] + "/soap");

            //this is the default binding that we need so we can access the service as /js or /jsdebug
            //use the configuration entry jsonBinding
            endpoint.Binding = new WebHttpBinding("jsonBinding");
            
            WebScriptEnablingBehavior behavior = new WebScriptEnablingBehavior();
            
            behavior.DefaultOutgoingResponseFormat = WebMessageFormat.Json;
            behavior.DefaultOutgoingRequestFormat = WebMessageFormat.Json;
            behavior.DefaultBodyStyle = WebMessageBodyStyle.WrappedRequest;
            endpoint.Behaviors.Add(behavior);
            endpoint.Behaviors.Add(new WebHttpBehaviorEx() { DefaultBodyStyle = WebMessageBodyStyle.WrappedRequest, AutomaticFormatSelectionEnabled = true, DefaultOutgoingRequestFormat = WebMessageFormat.Json, DefaultOutgoingResponseFormat = WebMessageFormat.Json });

            return host;
        }

        /// <summary>
        /// Iterates through an array of uris and returns the one that matches the current request.
        /// </summary>
        /// <param name="baseAddresses">An array of baseAddress which the application has site bindings for</param>
        /// <returns>The return array will always only have one element</returns>
        private Uri[] FilterBaseAddresses(Uri[] baseAddresses) {
            Uri[] retVal = null;
            foreach (var item in baseAddresses) {
                /* Find the appropriate host. We could also use LINQ to
                 * query for the item rather than iterate the array */
                if (string.Compare(item.Host, HttpContext.Current.Request.Url.Host, true) == 0) {
                    retVal = new Uri[1];
                    retVal[0] = item;
                    break;
                }
            }
            return retVal;
        }

    }
}
