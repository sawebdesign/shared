﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using SAWD.DataPusher.Common;
using System.Runtime.Serialization.Json;
using System.IO;
using SAWD.DataPusher;
using System.Threading;

namespace SAWD.DataPusher
{
    /// <summary>
    /// An asynchronous Http Handler that uses Comet style programming to implement a "server" that pushes data to a client. The DataPusherServer is implemented as an asynchronous handler
    /// so that when it us under heavy load it does not clog up IIS connections
    /// </summary>
    public class DataPusherServer : IHttpAsyncHandler
    {

        public DataPusherServer() {
        }

        #region IHttpAsyncHandler Members

        /// <summary>
        /// Called to initialize an asynchronous call to the HTTP handler. 
        /// </summary>
        /// <param name="context">An HttpContext that provides references to intrinsic server objects used to service HTTP requests.</param>
        /// <param name="cb">The AsyncCallback to call when the asynchronous method call is complete.</param>
        /// <param name="extraData">Any state data needed to process the request.</param>
        /// <returns>An IAsyncResult that contains information about the status of the process.</returns>
        public IAsyncResult BeginProcessRequest(HttpContext context, AsyncCallback cb, object extraData) {
            AsyncResult asyncres = new AsyncResult(context, cb, extraData);
            try {
                string data = readStream(context.Request.InputStream);
                var json = new DataContractJsonSerializer(typeof(PusherRequestData));
                var stream = new MemoryStream(Encoding.UTF8.GetBytes(data));
                PusherRequestData reqdata = (PusherRequestData)json.ReadObject(stream);                
                stream.Close();
               
                AsyncSymbolCheck symbolcheck = new AsyncSymbolCheck(asyncres, reqdata);

                ThreadStart ts = new ThreadStart(symbolcheck.CheckSymbols);
                Thread t = new Thread(ts);
                t.Start();
                return asyncres;
            }
            catch (Exception exc) {
                // will return an image with the error text
                SAWD.ExceptionHandler.WCFReturnMessage sawdexception = SAWD.ExceptionHandler.ExceptionProcessor.ProcessException(exc);
                asyncres.Exception = sawdexception;                
                asyncres.CompleteRequest();
                return asyncres;
            }
        }

        /// <summary>
        /// Provides an end method for an asynchronous process. 
        /// </summary>
        /// <param name="result">An IAsyncResult that contains information about the status of the process.</param>
        public void EndProcessRequest(IAsyncResult result) {
            if (!result.IsCompleted)
                return;
            string JSONresponse = string.Empty;
            if (result.GetType() == typeof(SAWD.DataPusher.Common.AsyncResult)) {
                //check if there was an error                
                SAWD.DataPusher.Common.AsyncResult asyncresult = ((SAWD.DataPusher.Common.AsyncResult)(result));
                HttpContext context = asyncresult.Context;
                if (asyncresult.Exception != null) {
                    context.Server.ClearError();
                    context.Response.Write(SerializeJSON(asyncresult.Exception));                    
                    context.Response.StatusCode = 200;
                    context.Response.End();
                }
                else {
                    context.Response.Write(SerializeJSON(asyncresult.ReturnData));
                    context.Response.End();
                }
            }

        }

        #endregion

        #region IHttpHandler Members

        public bool IsReusable {
            get { return true; }
        }

        /// <summary>
        /// The ProcessRequest method is implemented by HTTPHandlers and should never be called by Asynchronous calls
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context) {
            // should never get called
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        /// <summary>
        /// Private method used to serialize objects to JSON
        /// </summary>
        /// <param name="obj">Any object</param>
        /// <returns>A JSON serialization of obj</returns>
        private string SerializeJSON(object obj) {
            string retVal = string.Empty;
            
            MemoryStream ms = new MemoryStream();
            var json = new DataContractJsonSerializer(obj.GetType());
            json.WriteObject(ms, obj);
            retVal = Encoding.Default.GetString(ms.ToArray());
            ms.Close();
            ms.Dispose();

            return retVal;
        }

        /// <summary>
        /// Converts a stream to a string
        /// </summary>
        /// <param name="stream">A given stream</param>
        /// <returns>The string representation of the stream</returns>
        private string readStream(System.IO.Stream stream) {

            BinaryReader br = new BinaryReader(stream, System.Text.Encoding.UTF8);
            byte[] bytes = br.ReadBytes((int)stream.Length);
            return System.Text.Encoding.UTF8.GetString(bytes);
        }

    }
}
