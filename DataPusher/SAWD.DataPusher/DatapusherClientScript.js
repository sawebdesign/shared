/// <reference path="/scripts/MSAjaxExtension.js" />
/// <reference path="/scripts/MicrosoftAjax.js" />

/*globals Sys MSAjaxExtension ValidateObject*/

var SAWDDataPusher = {};
if (!SAWDDataPusher) {SAWDDataPusher = {}; }

SAWDDataPusher._selfConnection = {};

    SAWDDataPusher.DataPusherConnection = function (pusherurl, feedname, successcallback, errorcallback, wakeservertimeout, requestmessage) {

        if (successcallback !== undefined && successcallback !== null) {
            this.SuccessCallBack = successcallback;
        }
        if (errorcallback !== undefined && errorcallback !== null) {
            this.ErrorCallBack = errorcallback;
        }
        if (pusherurl !== undefined && pusherurl !== null) {
            this.URL = pusherurl;
        }
        if (wakeservertimeout !== undefined && wakeservertimeout !== null) {
            this.WakeserverTimeout = wakeservertimeout;
        }
        if (requestmessage !== undefined && requestmessage !== null) {
            this.RequestMessage = requestmessage;
        }
        if (feedname !== undefined && feedname !== null && this.RequestMessage !== null) {
            this.RequestMessage.FeedName = feedname;
        }
        SAWDDataPusher._selfConnection = this;
    };
    SAWDDataPusher.DataPusherConnection.prototype.SuccessCallBack = null;
    SAWDDataPusher.DataPusherConnection.prototype.ErrorCallBack = null;
    SAWDDataPusher.DataPusherConnection.prototype.RequestMessage = null;
    SAWDDataPusher.DataPusherConnection.prototype.URL = "";
    SAWDDataPusher.DataPusherConnection.prototype.WakeserverTimeout = 5000;
    SAWDDataPusher.DataPusherConnection.prototype.constructor = SAWDDataPusher.DataPusherConnection;
    SAWDDataPusher.DataPusherConnection.prototype.startSymbolRequest = function () {
        var wRequest = new Sys.Net.WebRequest();
        wRequest.set_url(SAWDDataPusher._selfConnection.URL);
        wRequest.set_httpVerb("POST");
        var body = Sys.Serialization.JavaScriptSerializer.serialize(SAWDDataPusher._selfConnection.RequestMessage);
        wRequest.set_body(body);
        wRequest.set_userContext(SAWDDataPusher._selfConnection);
        wRequest.get_headers()["Content-Length"] = body.length;        
        wRequest.invoke();
        wRequest.add_completed(function (executor, eventArgs) {
                                    var conn = executor.get_webRequest().get_userContext();
                                    SAWDDataPusher._selfConnection = conn;
                                    var result = executor.get_responseData();
                                    try {
                                        result = Sys.Serialization.JavaScriptSerializer.deserialize(result);
                                    }
                                    catch (e) {
                                        result = null;
                                    }
                                    if (result !== null) {
                                        var ajaxex = new MSAjaxExtension.AjaxException(result, false);
                                        if (ajaxex.FaultCode !== null) {
                                            //we now want to tell the server to send us back a message telling us it's back up
                                            //we don't check prices because prices could be the same in which case the server won't send a message back telling us it is back up                    
                                            conn.WakeServer = true;
                                            setTimeout(conn.startSymbolRequest, conn.WakeserverTimeout);
                                            conn.ErrorCallBack(ajaxex, conn.WakeserverTimeout, SAWDDataPusher._selfConnection);
                                            return;
                                        }
                                        var returnSymbols = new SAWDDataPusher.AjaxPushDataList(result);
                                        conn.SuccessCallBack(returnSymbols, conn.RequestMessage.Symbols, SAWDDataPusher._selfConnection);
                                        conn.WakeServer = false;
                                        //update the existing symbols to the new values received so that we can resubmit the request
                                        for (var i = 0; i < returnSymbols.DataList.length; i++) {
                                            var newsymbol = returnSymbols.DataList[i];
                                            for (var j = 0; j < conn.RequestMessage.Symbols.DataList.length; j++) {
                                                var existsymbol = conn.RequestMessage.Symbols.DataList[j];
                                                if (existsymbol.Symbol == newsymbol.Symbol) {
                                                    existsymbol.Data = newsymbol.Data;
                                                    break;
                                                }
                                            }
                                        }
                                        conn.startSymbolRequest();
                                    }
                                });
    };




    SAWDDataPusher.DataPusherBase = function () {
        var t = this;
    };
    SAWDDataPusher.DataPusherBase.prototype.entityHashCode = null;
    SAWDDataPusher.DataPusherBase.prototype.validate = function () {
        return ValidateObject(this);
    };
    SAWDDataPusher.DataPusherBase.prototype.clone = function (source) {
        if (source !== undefined) {
            if (source !== null) {
                for (var i in source) {
                    if (true)
                    { this[i] = source[i]; }
                }
            }
        }
    };




    SAWDDataPusher.AjaxPushDataList = function (wo) {
        var t = this;
        t.__type = "AjaxPushDataList:#SAWD.DataPusher.Common";
        //assign all the properties from the referencing object
        this.clone(wo);
    };

    SAWDDataPusher.AjaxPushDataList.prototype = new SAWDDataPusher.DataPusherBase();
    SAWDDataPusher.AjaxPushDataList.prototype.constructor = SAWDDataPusher.AjaxPushDataList;
    SAWDDataPusher.AjaxPushDataList.prototype.LastServerMessage = new Date();
    SAWDDataPusher.AjaxPushDataList.prototype.ReturnClientId = null;
    SAWDDataPusher.AjaxPushDataList.prototype.DataList = [];

    SAWDDataPusher.AjaxPushData = function (wo, symbol, data) {
        var t = this;
        t.__type = "AjaxPushData:#SAWD.DataPusher.Common";
        if (symbol !== undefined && data !== undefined) {
            this.Symbol = symbol;
            this.Data = data;
        }
        else {
            //assign all the properties from the referencing object
            this.clone(wo);
        }
    };

    SAWDDataPusher.AjaxPushData.prototype = new SAWDDataPusher.DataPusherBase();
    SAWDDataPusher.AjaxPushData.prototype.constructor = SAWDDataPusher.AjaxPushData;
    SAWDDataPusher.AjaxPushData.prototype.Symbol = null;
    SAWDDataPusher.AjaxPushData.prototype.Data = null;
    SAWDDataPusher.AjaxPushData.prototype.LastUpdateTime = new Date();



    SAWDDataPusher.PusherRequestData = function (wo) {
        var t = this;
        t.__type = "PusherRequestData:#SAWD.DataPusher.Common";
        //assign all the properties from the referencing object
        this.clone(wo);
    };

    SAWDDataPusher.PusherRequestData.prototype = new SAWDDataPusher.DataPusherBase();
    SAWDDataPusher.PusherRequestData.prototype.constructor = SAWDDataPusher.PusherRequestData;
    SAWDDataPusher.PusherRequestData.prototype.Symbols = {};
    SAWDDataPusher.PusherRequestData.prototype.Symbols.DataList = [];
    SAWDDataPusher.PusherRequestData.prototype.FeedName = "";
    SAWDDataPusher.PusherRequestData.prototype.ClientParams = [];
    SAWDDataPusher.PusherRequestData.prototype.WakeServer = false;

    SAWDDataPusher.ClientParam = function (wo, key, value) {
        var t = this;
        t.__type = "ClientParam:#SAWD.DataPusher.Common";
        //assign all the properties from the referencing object
        if (key !== undefined && value !== undefined) {
            this.Key = key;
            this.Value = value;
        }
        else {
            this.clone(wo);
        }
    };

    SAWDDataPusher.ClientParam.prototype = new SAWDDataPusher.DataPusherBase();
    SAWDDataPusher.ClientParam.prototype.Value = null;
    SAWDDataPusher.ClientParam.prototype.Key = null;
    SAWDDataPusher.ClientParam.prototype.constructor = SAWDDataPusher.ClientParam;

