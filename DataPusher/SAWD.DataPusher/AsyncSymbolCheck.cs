﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using SAWD.DataPusher.Common;
using System.Configuration;
using System.Reflection;
using System.Threading;

namespace SAWD.DataPusher
{
    /// <summary>
    /// Class that is typically called in a new thread and will check a given "Feed" to check if a list of Symbol|Data values have changed
    /// Because this class's CheckSymbols method is typically called using the new Thread method and so paramters cannot be passed to it, we have the PusherRequestData and AsyncResult properties set in its constructor
    /// </summary>
    public class AsyncSymbolCheck
    {

        public AsyncSymbolCheck(AsyncResult ars, PusherRequestData rqdata) {
            asyncResult = ars;
            requestdata = rqdata;
        }


        private AsyncResult asyncResult;
        private PusherRequestData requestdata;

        /// <summary>
        /// Method that instantiates a "feed" class via reflection and then periocically checks the symbols in that feed for any changes
        /// If changes are detected it calls the CompleteRequest method of the calling AsyncResult to notify the thread that it has completed
        /// </summary>
        public void CheckSymbols() {

            IAjaxPushDataList symbols = requestdata.Symbols;
            string feedname = requestdata.FeedName;
            Dictionary<string, string> clientparams = requestdata.ClientParams;
            AjaxPushDataList retVal = new AjaxPushDataList();

            //check if we only want to see if the server is back up and running
            if (requestdata.WakeServer) {                
                if (asyncResult != null) {
                    retVal.LastServerMessage = DateTime.Now;
                    asyncResult.ReturnData = retVal;
                    asyncResult.CompleteRequest();
                    return;
                }
            }

            string threadID = string.Empty;
            if (clientparams != null) {
                if (clientparams.ContainsKey("threadID"))
                    threadID = clientparams["threadID"].ToString();
            }
            retVal.ReturnClientId = threadID;
            bool FoundChange = false;
            int totaltime = 0;
            PushDataConfigSection section = (PushDataConfigSection)ConfigurationManager.GetSection("PushDataServiceFeeds");
            FeedConfigElement feedSettings = section.PushDataFeeds[feedname];
            Dictionary<string, string> configparams = null;
            if (feedSettings.Params != null)
                configparams = feedSettings.Params.ToDictionary();
            //IPushDataFeed pushDataFeed = new Juice.Feeds.TestPriceCheck();
            //Create an instance of the class specified by the feedsettings
            //we know all feeds must implement IPushDataFeed, so we can cast it as such
            IPushDataFeed pushDataFeed = (IPushDataFeed)(Activator.CreateInstance(feedSettings.Type.Split(',')[1], feedSettings.Type.Split(',')[0]).Unwrap());
             
            while (!FoundChange && (totaltime < feedSettings.Timeout || feedSettings.Timeout == 0)) {
                //check what the cache holds
                //pushDataFeed.Context = context;
                IAjaxPushDataList cachelistlist = pushDataFeed.GetSymbolList(clientparams, configparams);
                foreach (AjaxPushData item in symbols.DataList) {
                    //find the symbol in the cachelist and check if the data is different
                    AjaxPushData cacheitem = cachelistlist.FindBySymbol(item.Symbol);
                    if (cacheitem != null) {
                        if (item.Symbol == cacheitem.Symbol && item.Data != cacheitem.Data) {
                            retVal.DataList.Add(cacheitem);
                        }
                    }
                }
                if (retVal.DataList.Count == 0) {
                    totaltime += feedSettings.Sleep;
                    System.Threading.Thread.Sleep(feedSettings.Sleep);
                }
                else
                    FoundChange = true;
            }

            if (asyncResult != null) {
                retVal.LastServerMessage = DateTime.Now;
                asyncResult.ReturnData = retVal;

                asyncResult.CompleteRequest();
            }
        }

    }


}
