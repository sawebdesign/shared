﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SAWD.DataPusher
{
    public sealed class StatCounter
    {
        private static volatile StatCounter instance;
        private static object syncRoot = new Object();

        private StatCounter() { }

        public static StatCounter Instance {
            get {
                if (instance == null) {
                    lock (syncRoot) {
                        if (instance == null) {
                            instance = new StatCounter();
                            //instance.FirstTimeMessages = new List<string>();
                            //instance.SecondTimeMessages = new List<string>();
                            //instance.ErroredReturns = 0;
                            instance.TotalThreads = 0;
                        }
                    }
                }

                return instance;
            }
        }

        public int TotalThreads { get; set; }
        //public int FirstTimeReturns { get; set; }
        //public int SecondTimeReturns { get; set; }
        //public int ErroredReturns { get; set; }

        //public List<string> FirstTimeMessages { get; set; }
        //public List<string> SecondTimeMessages { get; set; }
    }
}
