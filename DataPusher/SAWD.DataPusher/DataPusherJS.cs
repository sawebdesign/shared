﻿using System;
using System.Web;
using SAWD.DataPusher.Common;
using System.Configuration;
using System.IO;
using System.Reflection;

namespace SAWD.DataPusher
{
    public class DataPusherJS : IHttpHandler
    {
        /// <summary>
        /// You will need to configure this handler in the web.config file of your 
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        public bool IsReusable {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return true; }
        }

        public void ProcessRequest(HttpContext context) {
            PushDataConfigSection section = (PushDataConfigSection)ConfigurationManager.GetSection("PushDataServiceFeeds");
            if (section.UseMinifiedJS)
                context.Response.Write(GetResource("SAWD.DataPusher.DatapusherClientScript.min.js"));
            else
                context.Response.Write(GetResource("SAWD.DataPusher.DatapusherClientScript.js"));
        }

        private Stream GetResourceStream(string resourceName) {
            return Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName);
        }

        private string GetResource(string resourceName) {
            return new StreamReader(GetResourceStream(resourceName)).ReadToEnd();
        }

        #endregion
    }
}
