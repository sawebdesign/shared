﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SAWD.DataPusher.Common
{
    public class PushDataConfigSection : ConfigurationSection
    {
        /// <summary>
        /// A collection of Feed configurations used by the application
        /// </summary>
        [ConfigurationProperty("Feeds")]
        public FeedElementCollection PushDataFeeds {
            get { return ((FeedElementCollection)(base["Feeds"])); }
        }

        /// <summary>
        /// Whether or not the minified version of the DataPusher javascript should be used when using the DataPusherJS handler
        /// </summary>
        [ConfigurationProperty("useminifiedJS", DefaultValue = "false", IsKey = false, IsRequired = false)]
        public bool UseMinifiedJS {
            get {
                return ((bool)(base["useminifiedJS"]));
            }
            set {
                base["useminifiedJS"] = value;
            }
        }
    }

}
