﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SAWD.DataPusher.Common
{
    [ConfigurationCollection(typeof(FeedConfigElement))]
    public class FeedElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement() {

            return new FeedConfigElement();

        }



        protected override object GetElementKey(ConfigurationElement element) {

            return ((FeedConfigElement)(element)).Name;

        }



        public FeedConfigElement this[int idx] {
            get {
                return (FeedConfigElement)BaseGet(idx);
            }
        }

        
        public FeedConfigElement this[string name] {
            get {
                return (FeedConfigElement)BaseGet(name);
            }
        }

    }
}
