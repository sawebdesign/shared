﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SAWD.DataPusher.Common
{
    public class FeedConfigElement : ConfigurationElement 
    {
        /// <summary>
        /// The feed name
        /// </summary>
        [ConfigurationProperty("name", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string Name {
            get {
                return ((string)(base["name"]));
            }
            set {
                base["name"] = value;
            }
        }

        /// <summary>
        /// The type of feed. This should be a valid Namespace/Assembly notation e.g. "Juice.Feeds.TestPriceCheck, Juice.Feeds"
        /// </summary>
        [ConfigurationProperty("type", DefaultValue = "", IsKey = false, IsRequired = true)]
        public string Type {
            get {
                return ((string)(base["type"]));
            }
            set {
                base["type"] = value;
            }
        }

        /// <summary>
        /// The timeout before the "registration" call should send an empty response back to the client so that it can re-request
        /// </summary>
        [ConfigurationProperty("timeout", DefaultValue = "60000", IsKey = false, IsRequired = true)]
        public int Timeout {
            get {
                return ((int)(base["timeout"]));
            }
            set {
                base["timeout"] = value;
            }
        }

        /// <summary>
        /// How long the feed sleeps for in between checking its symbols
        /// </summary>
        [ConfigurationProperty("sleep", DefaultValue = "500", IsKey = false, IsRequired = true)]
        public int Sleep {
            get {
                return ((int)(base["sleep"]));
            }
            set {
                base["sleep"] = value;
            }
        }

        /// <summary>
        /// A collection of paramters specific to the feed
        /// </summary>
        [ConfigurationProperty("params")]
        public ParamElementCollection Params {
            get { return ((ParamElementCollection)(base["params"])); }
        }

    }
   
}
