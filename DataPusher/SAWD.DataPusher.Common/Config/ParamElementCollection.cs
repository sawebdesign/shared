﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SAWD.DataPusher.Common
{

    [ConfigurationCollection(typeof(ParamConfigElement))]
    public class ParamElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement() {

            return new ParamConfigElement();

        }



        protected override object GetElementKey(ConfigurationElement element) {

            return ((ParamConfigElement)(element)).Name;

        }



        public ParamConfigElement this[int idx] {
            get {
                return (ParamConfigElement)BaseGet(idx);
            }
        }


        public ParamConfigElement this[string name] {
            get {
                return (ParamConfigElement)BaseGet(name);
            }
        }

        public Dictionary<string, string> ToDictionary() {
            Dictionary<string, string> retval = new Dictionary<string, string>();
            for (int i = 0; i < this.Count; i++) {
                retval.Add(this[i].Name, this[i].Value);
            }
            return retval;

        }

    }
}
