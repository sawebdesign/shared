﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SAWD.DataPusher.Common
{
    /// <summary>
    /// An interface that all customised feeds must implement. Because the DataPusher handler uses reflection to load the appropriate feed each feed must implement a common interface so that we can call common methods.
    /// </summary>
    public interface IPushDataFeed
    {
        /// <summary>
        /// Method that returns an instance of IAjaxPushDataList when changes are found
        /// </summary>
        /// <param name="callerparams">Parameters sent by the client application</param>
        /// <param name="configparams">Parameters taken from the server configuration file</param>
        /// <returns></returns>
        IAjaxPushDataList GetSymbolList(Dictionary<string, string> callerparams, Dictionary<string, string> configparams);
        
        /// <summary>
        /// Usually the current HTTPContext
        /// </summary>
        System.Web.HttpContext Context { get; set; }
    }
}
