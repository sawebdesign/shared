﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SAWD.DataPusher.Common
{
    /// <summary>
    /// Class representing the Request that a client makes to the DataPusher server
    /// </summary>
    [DataContract]
    [Serializable]
    public class PusherRequestData
    {
        /// <summary>
        /// A list of Symbols that the client registers to receive updates for
        /// </summary>
        [DataMember]        
        public AjaxPushDataList Symbols { get; set; }

        /// <summary>
        /// The feed that the DataPusher shoudl get the data from
        /// </summary>
        [DataMember]
        public string FeedName { get; set; }

        /// <summary>
        /// A collection of key/value pairs that are sent up by the client. These are passed to the feed
        /// </summary>
        [DataMember]
        public Dictionary<string, string> ClientParams { get; set; }

        /// <summary>
        /// A boolean that defines whether or not the request is only there to check if the server is "awake" i.e. this request is made when the server has gone down and the client wants to check if it back up
        /// </summary>
        [DataMember]
        public bool WakeServer { get; set; }
    }
}
