﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SAWD.DataPusher.Common
{
    /// <summary>
    /// A class representing a message that the DataPusher server must send to a connected client
    /// </summary>
   [DataContract]
    [Serializable]
    public class AjaxPushDataList : IAjaxPushDataList
    {
        private List<AjaxPushData> datalist = new List<AjaxPushData>();

        /// <summary>
        /// Date when this message was sent
        /// </summary>
        [DataMember]
        public DateTime LastServerMessage { get; set; }

        /// <summary>
        /// The client ID that sent the request
        /// </summary>
        [DataMember]
        public string ReturnClientId { get; set; }

        /// <summary>
        /// A list of AjaxPushData objects that have changed
        /// </summary>
        [DataMember]
        public List<AjaxPushData> DataList { get { return datalist; } set { datalist = value; } }

        public AjaxPushData FindBySymbol(string symbol) {
            AjaxPushData retItem = this.DataList.Find(delegate(AjaxPushData item) { return item.Symbol == symbol; });
            return retItem;
        }

    } 
        
    /// <summary>
    /// Class representing a symbol and its associated data and modified date and time. 
    /// </summary>  
    [DataContract]
    [Serializable]
    public class AjaxPushData : IAjaxPushData
    {
        /// <summary>
        /// The Symbol can be seen as an identifier of a piece of information e.g. a currency symbol like ZAR 
        /// </summary>
        [DataMember]
        public string Symbol { get; set; }

        /// <summary>
        /// The data associated with the symbol. This could typically be a stock price or betting odds
        /// </summary>
        [DataMember]
        public string Data { get; set; }

        /// <summary>
        /// The date and time that this symbol was recorded as having changed
        /// </summary>
        [DataMember]
        public DateTime LastUpdateTime { get; set; }
    }

}
