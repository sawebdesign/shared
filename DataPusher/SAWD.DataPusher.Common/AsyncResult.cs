﻿using System;
using System.Threading;
using System.Web;

namespace SAWD.DataPusher.Common
{
    /// <summary>
    /// The result of an asynchronous operation.
    /// </summary>
    public class AsyncResult : IAsyncResult
    {
        public AsyncResult(HttpContext ctx,
                                 AsyncCallback cb,
                                 object extraData) {
                                     Context = ctx;
            _cb = cb;
            _extraData = extraData;
        }

        public HttpContext Context;
        internal AsyncCallback _cb;
        internal object _extraData;
        private bool _isCompleted = false;
        private ManualResetEvent _callCompleteEvent = null;


        public AjaxPushDataList ReturnData { get; set; }
        public SAWD.ExceptionHandler.WCFReturnMessage  Exception { get; set; }

        public void CompleteRequest() {
            _isCompleted = true;
            lock (this) {
                if (_callCompleteEvent != null)
                    _callCompleteEvent.Set();
            }
            // if a callback was registered, invoke it now
            if (_cb != null)
                _cb(this);
        }

        // IAsyncResult interface property implementations
        public object AsyncState { get { return (_extraData); } }
        public bool CompletedSynchronously { get { return (false); } }
        public bool IsCompleted { get { return (_isCompleted); } }
        public WaitHandle AsyncWaitHandle {
            get {
                lock (this) {
                    if (_callCompleteEvent == null)
                        _callCompleteEvent = new ManualResetEvent(false);

                    return _callCompleteEvent;
                }
            }
        }
    }
}
