﻿using System;
using System.Runtime.Serialization;
namespace SAWD.DataPusher.Common
{
    /// <summary>
    /// An interface representing a message that the DataPusher server must send to a connected client
    /// </summary>
    public interface IAjaxPushDataList
    {
        /// <summary>
        /// A list of AjaxPushData objects that have changed
        /// </summary>
        [DataMember]
        System.Collections.Generic.List<AjaxPushData> DataList { get; set; }
       
        AjaxPushData FindBySymbol(string symbol);

        /// <summary>
        /// Date when this message was sent
        /// </summary>
        [DataMember]
        DateTime LastServerMessage { get; set; }

        /// <summary>
        /// The client ID that sent the request
        /// </summary>
        [DataMember]
        string ReturnClientId { get; set; }

    }
}
