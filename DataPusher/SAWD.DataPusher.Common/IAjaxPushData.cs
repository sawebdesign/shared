﻿using System;
using System.Runtime.Serialization;
namespace SAWD.DataPusher.Common
{
    /// <summary>
    /// Interface representing a symbol and its associated data and modified date and time. 
    /// </summary>
    public interface IAjaxPushData
    {
        /// <summary>
        /// The Symbol can be seen as an identifier of a piece of information e.g. a currency symbol like ZAR 
        /// </summary>
        [DataMember]
        string Data { get; set; }

        /// <summary>
        /// The data associated with the symbol. This could typically be a stock price or betting odds
        /// </summary>
        [DataMember]
        DateTime LastUpdateTime { get; set; }

        /// <summary>
        /// The date and time that this symbol was recorded as having changed
        /// </summary>
        [DataMember]
        string Symbol { get; set; }
    }
}
